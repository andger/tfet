%% DG TFET transmittance computation by 2 WKB integrals
%{
    Based on "Short-channel effects in tunnel FETs," by Wu, Min, Taur.

    TFET structure:
        * Source  -> AlGaAsSb   (p-doped  <->  Na = 3e19 cm^-3)
        * Channel -> InGaAs     (intrinsic)
        * Drain   -> AlGaAsSb   (n-doped  <->  Nd = 3e19 cm^-3)

    Assumptions and approximations:
        * 1D potential
        * m_eln = m_hol
        * epsilon_s = epsilon_ox

    From structure and mass approximation:
        * densities of states of CB, VB  -> Nc = Nv
        * degeneracy of source and drain -> d1 = d2
%}

clear;
clc;

%% Fundamental constants
m0 = 9.1095e-31;                          % [kg], electron mass
qel = 1.602177e-19;                       % [C], elementary charge
kb = 8.617330350e-5;                      % [eV/K], Boltzmann constant
h = 6.626069e-34;                         % [J*s], Planck constant
hbar = h / (2 * pi);                      % [J*s], reduced Planck constant
epsilon0 = 8.854e-14;                     % [F/cm], dielectric constant

nmtom = 1e-9;                             % [nm -> m]
nmtocm = 1e-7;                            % [nm -> m]
cmtom = 1e-2;                             % [cm -> m]

%% Device parameters
L = 20;                                   % [nm], channel length
Temp = 300;                               % [K], operating temperature
epsilonr = 14.6;                          % [-], relative dielectric constant
epsilon = epsilonr * epsilon0;
m = 0.1 * m0;                             % [kg], tunneling mass, assuming me = mh
ts = 5;                                   % [nm], semiconductor thickness
ti = 2;                                   % [nm], insulator thickness
lambda = ts + 2 * ti;                     % [nm], tunneling window length
Na = 3e19;                                % [cm^-3], source doping level
Nd = Na;                                  % [cm^-3], drain doping level
Nv = 2e19;                                % [cm^-3], VB effective DOS
Nc = 2e19;                                % [cm^-3], CB effective DOS

V1 = 0.23;                                % [eV], effective band gap
tmp1 = sqrt(pi)/2 * Na/Nv;
tmp2 = sqrt(pi)/2 * Nd/Nc;
d1 = kb * Temp * approx_ferinv(tmp1);     % [eV], source degeneracy
d2 = kb * Temp * approx_ferinv(tmp2);     % [eV], drain degeneracy

N = 100;                                  % number of nodes

%% Structs to more easily handle the passing to functions
TFET.L = L;                               % [nm]
TFET.ts = ts * nmtocm;                    % [cm]
TFET.tox = ti * nmtocm;                   % [cm]
TFET.lambda = lambda;                     % [nm]
TFET.Temp = Temp;                         % [K]
TFET.epsilon = epsilon;                   % [F/cm]
TFET.m = m;                               % [kg]
TFET.Na = Na;                             % [cm^-3]
TFET.Nd = Nd;                             % [cm^-3]
TFET.Nv = Nv;                             % [cm^-3]
TFET.Nc = Nc;                             % [cm^-3]
TFET.V1 = V1;                             % [eV]
TFET.d1 = d1;                             % [eV]
TFET.d2 = d2;                             % [eV]

%% Trans-characteristic
% V0 -> [eV] gate control of channel CB, determines tunneling window
% delta -> [eV] band bending

% Applied bias
% Vgs_vec = linspace(-0.1, 0.5, 30).';          % [V]
% Vds_vec = 0.5;                                % [V]
Vds_vec = linspace(0, 0.5, 30).';           % [V]
Vgs_vec = 0.5;                              % [V]

H = zeros(N, 1);
A = zeros(N, 1);
T = zeros(N, 1);
I = zeros(length(Vds_vec), length(Vgs_vec));

% Transmittance loop
alpha = (2/hbar)*sqrt(2*m);              % unit of meas is 1/J^0.5/m
beta = sqrt(epsilon/(2*qel^2*Na));       % unit of meas is cm/J^0.5

T_multi = -2*sqrt(2)/hbar;                     % pre-factor for WKB integral
i_coeff = (m*qel)./(2*(pi^2)*(hbar^3));        % pre-factor for current

tic

for iD = 1 : length(Vds_vec)
    Vds = Vds_vec(iD);
    V2 = Vds + d1 + d2;                       % [eV], drain conduction band
    
    for iG = 1 : length(Vgs_vec)
        Vgs = Vgs_vec(iG);
        
        % De-bias model
        Vgs = DeBiasModel(TFET, Vgs, Vds);
        [V0, delta] = V0compute(TFET, Vgs, Vds);
        
        dE = abs(V2)/90;
        E_vec = 0+dE/1000000:dE:V2-dE/1000000;       % [eV], VB eln energies
        T2D_vec = zeros(size(E_vec));
        
        TpartE_vec = zeros(size(E_vec));
        TpartEt_vec = zeros(size(E_vec));
        Et_vec = zeros(size(E_vec));
        
        Integrand = zeros(size(E_vec));
        for iE = 1 : length(E_vec)
            E = E_vec(iE);
            maxEtr = V2 - E;
            Etrm = min(E, maxEtr);       % integral upper limit of Etr
            
            dEtr = abs(Etrm - 0)/90;
            E_trans_vec = 0 : dEtr : Etrm-dEtr/100000;
            
            TE_vec = zeros(size(E_trans_vec));
            
            % Evaluate WKB integrals
            for iE_trans = 1:length(E_trans_vec)
                E_tr = E_trans_vec(iE_trans);
                x_channel = 0:L/1000:L;
                
                % Analytical solution for U(x)
                %     U(x) source VB profile
                T0S = sqrt(epsilon/cmtom/2/Na*cmtom^3)*(sqrt(delta*(delta-(E-E_tr))) ...
                    -(E-E_tr)*log(sqrt(delta/(E-E_tr))+sqrt((delta-(E-E_tr))/(E-E_tr))));
                T0_S = - alpha * T0S;
                
                % Numerical solution for V(x)
                %     V(x) channel CB profile
                l2 = integration_limit(E + E_tr, x_channel, V0, delta, TFET, Vds); % [nm]
                
                dx = abs(l2-0)/200;
                x = (dx/10000):dx:(l2-dx/10000);                                   % [nm]
                Vx = (V0*sinh(pi*(L-x)/lambda)./sinh(pi*L/lambda)-V0+V1-delta...
                    -(V2-V0+V1-delta).*sinh(pi*x/lambda)./sinh(pi*L/lambda));     % [eV]
                
                VarA = (abs(m * qel * (Vx + E + E_tr))).^(1/2);
                T_partEin = sum(VarA) * (1e-9 * dx);
                
                T_partE = exp(T_multi .* T_partEin .* (E>=delta-V1-E_tr) + T0_S .* (E<=delta+E_tr));
                TE_vec(iE_trans) = T_partE;
                T2D = sum(TE_vec) .* (qel * dEtr);   % [J]
            end
            
            % Defining Fermi Level
            Efs = d1;        %[eV] Source Fermi Level
            Efd = Vds + d1;  %[eV] Drain Fermi Level
            
            % Define quasi-Fermi level for source
            fs = 1/(1+exp((Efs-E)/kb/Temp));
            % Define quasi-Fermi level for drain
            fd = 1/(1+exp((Efd-E)/kb/Temp));
            
            % Calculate each element of the integral with a given E
            Integrand(iE) = (fs-fd) .* T2D;
        end
        
        % Numerical integration of Landauer
        Ie = i_coeff * sum(Integrand) * dE*qel;
        I(iD,iG) = Ie*5e-15;                % unit of A/um, with body thickness of 5 nm
    end
end

toc

if length(Vgs_vec) > length(Vds_vec)
    semilogy(Vgs_vec, I, 'LineWidth', 1.25);
    hold on
    axis([min(Vgs_vec) max(Vgs_vec) 1e-11 1e-3]);
    title('Trans-characteristic', 'Interpreter', 'latex');
    xlabel('$V_\textup{gs}$ [V]', 'Interpreter', 'latex');
    legend({'$1\times 10^{19}$ cm$^{-3}$', '$3\times 10^{19}$ cm$^{-3}$','$5\times 10^{19}$ cm$^{-3}$'}, ...
        'Interpreter', 'latex', 'Location', 'best', 'Box', 'off', 'FontSize', 11)
    print('trans-doping', '-dpng')
else
    plot(Vds_vec, I, 'LineWidth', 1.25);
    hold on;
    axis([min(Vds_vec) max(Vds_vec) 1e-11 1e-3]);
    title('Output-characteristic', 'Interpreter', 'latex');
    xlabel('$V_\textup{ds}$ [V]', 'Interpreter', 'latex');
    legend({'$1\times 10^{19}$ cm$^{-3}$', '$3\times 10^{19}$ cm$^{-3}$', '$1\times 10^{20}$ cm$^{-3}$'}, ...
        'Interpreter', 'latex', 'Location', 'best', 'Box', 'off', 'FontSize', 11)
    print('out-doping', '-dpng')
end
ylabel('$I_\textup{ds}$ [A/$\mu$m]', 'Interpreter', 'latex');
grid on;

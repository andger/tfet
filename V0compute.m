function [V0, delta] = V0compute(TFET, Vgs, Vds)
    
    % Fundamental constants
    qel = 1.602177e-19;                         % [C], electron charge
    nmtocm = 1e-7;                              % [nm -> cm]
    
    % Technological device parameters
    epsilon = TFET.epsilon;                     % [F/cm], dielectric constant
    Na = TFET.Na;                               % [cm^-3], acceptor doping
    L = TFET.L;                                 % [nm], device length
    lambda = TFET.lambda;                       % [nm], tunneling window length
    
    % Potential parameters
    V1 = TFET.V1;                               % [eV], effective band gap
    V2 = Vds + TFET.d1 + TFET.d2;               % [eV], potential at drain
    
    V0 = zeros(size(Vgs));
    for i = 1 : length(Vgs)
        fun = @(V0) (pi/lambda/nmtocm)*((V0*cosh(pi*L/lambda)+V2-Vgs(i))/(sinh(pi*L/lambda)))-sqrt(qel)*sqrt(max(0, 2*Na*(Vgs(i)+V1-V0)/epsilon));
        V_0 = 0.1;
        V0(i) = fzero(fun, V_0);
    end
    
    delta = Vgs - V0 + V1;
    if delta < 0
        delta = 0;
        V0 = Vgs + V1;
    end
end